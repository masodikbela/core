// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "account_cache.hpp"

#include "../logger.hpp"
#include "../mysql/migrations/_defaults.hpp"

namespace core::cache {
    AccountCache::AccountCache(std::shared_ptr<Redis> redis,
                               std::shared_ptr<mysql::MySQL> database)
        : _redis(std::move(redis)), _database(std::move(database)) {}

    AccountCache::~AccountCache() = default;

    uint8_t AccountCache::GetEmpire(uint32_t id) {
        uint8_t empire = 0;

        auto key = "account:" + std::to_string(id);
        auto exists = _redis->Execute<int64_t>(
            bredis::single_command_t{"hexists", key, "empire"});
        if (exists == 0) {
            CORE_LOGGING(trace) << "Account empire not cached";

            auto stmt = _database->CreateStatement(
                "SELECT `empire` FROM `" ACCOUNT_DATABASE
                "`.`accounts` WHERE `id` = %1%");
            stmt << id;

            auto result = stmt();

            if (result->Next()) {
                empire = result->GetUnsigned8("empire");

                _redis->Execute(bredis::single_command_t{
                    "hset", key, "empire", std::to_string(empire)});
            } else {
                CORE_LOGGING(error) << "Account not found!";
                return 0;
            }
        } else {
            auto value = boost::get<string_t>(_redis->Execute(
                bredis::single_command_t{"hget", key, "empire"}));
            empire =
                static_cast<uint8_t>(boost::lexical_cast<uint32_t>(value.str));
        }

        return empire;
    }

    void AccountCache::SetEmpire(uint32_t id, uint8_t empire) {
        auto key = "account:" + std::to_string(id);
        _redis->Execute(bredis::single_command_t{"hset", key, "empire",
                                                 std::to_string(empire)});
    }
}  // namespace core::cache
