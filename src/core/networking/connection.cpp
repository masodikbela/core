// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "connection.hpp"

#include <boost/bind.hpp>
#include <boost/exception/diagnostic_information.hpp>
#include <boost/format.hpp>
#include <chrono>
#include <thread>
#include <utility>
#include <vector>

#include "../application.hpp"
#include "../profiler.hpp"
#include "packet.hpp"
#include "packet_manager.hpp"
#include "reserved_headers.hpp"
#include "server.hpp"

namespace core::networking {
    Connection::Connection(boost::asio::io_context &context,
                           std::shared_ptr<Server> server,
                           uint8_t securityLevel, uint32_t *defaultKey)
        : _strand(boost::asio::make_strand(context.get_executor())),
          _socket(_strand),
          _isClosing(false),
          _isShutingDown(false),
          _buffer(),
          _server(std::move(server)),
          _securityLevel(securityLevel),
          _handshaking(false),
          _handshake(1337),
          _phase(0),
          _id(0) {
        if (securityLevel == SECURITY_LEVEL_KEY_AGREEMENT) {
            _cryptation = std::make_unique<KeyAgreementCryptation>();
        } else if (securityLevel == SECURITY_LEVEL_XTEA) {
            _cryptation = std::make_unique<XTEACryptation>();
            _cryptation->AddData(XTEA_CRYPTATION_START_KEY, defaultKey, 16);
        }
    }

    Connection::~Connection() {
        CORE_LOGGING(trace) << "Destroying connection object";
    }

    void Connection::Start() {
        CORE_LOGGING(debug) << "New connection from "
                            << _socket.remote_endpoint().address().to_string();
        ReadNextPacket();

        if (_securityLevel >= SECURITY_LEVEL_NONE) {
            StartHandshake();
        } else {
            _server->CallNewConnectionHandler(
                shared_from_this());  // Skip handshake and switch to the next
                                      // phase
        }
    }

    void Connection::PostClose() {
        // this supposed to be called outside of the network thread
        if (_isClosing.exchange(true)) return;
        boost::asio::post(_strand,
                          boost::bind(&Connection::Close, shared_from_this()));
    }

    void Connection::PostShutdown() {
        if (_isClosing || _isShutingDown.exchange(true)) return;
        boost::asio::post(_strand,
                          boost::bind(&Connection::Shutdown, shared_from_this()));
    }

    void Connection::Close() {
        // this supposed to be always called from the network thread
        _isClosing = true;
        if (_socket.is_open()) {
            CORE_LOGGING(debug)
                << "Closing connection to "
                << _socket.remote_endpoint().address().to_string();
            boost::system::error_code ec;
            _socket.close(ec);
            if (ec) {
                CORE_LOGGING(info) << "Failed to close connection: " << ec.message();
            }
        }

        if (_id > 0) {
            _server->CallDisconnectHandler(shared_from_this());
            _server->RemoveConnection(_id);
            _id = 0;
        }
    }

    void Connection::Shutdown() {
        _isShutingDown = true;
        if (_socket.is_open()) {
            CORE_LOGGING(trace)
                << "Shuting down connection to "
                << _socket.remote_endpoint().address().to_string();
            boost::system::error_code ec;
            _socket.shutdown(boost::asio::socket_base::shutdown_both, ec);
            if (ec) {
                CORE_LOGGING(info)
                    << "Failed to shutdown connection: " << ec.message();
            }
        }
    }

    void Connection::Send(std::shared_ptr<Packet> packet) {
        if (_isClosing) return;
        // this send is thread safe, can be called from any thread
        std::lock_guard<std::mutex> guard(_sendMutex);
        _sendQueue.push(packet);
        // if the send queue has data before push it means that the HandleSend
        // is already posted or its running from the HandleWrite
        if (_sendQueue.size() == 1) {
            boost::asio::post(_strand, boost::bind(&Connection::HandleSend,
                                                   shared_from_this()));
        }
    }

    void Connection::HandleSend() {
        if (_isClosing || !_socket.is_open()) return;
        std::shared_ptr<core::networking::Packet> packet(nullptr);
        {
            std::lock_guard<std::mutex> guard(_sendMutex);
            if (_sendQueue.empty()) return;
            packet = _sendQueue.front();
        }

        CORE_LOGGING(trace) << "Sending packet " << packet->GetName() << " ("
                            << (uint32_t)packet->GetHeader() << ")";

        uint8_t header = packet->GetHeader();
        auto data = packet->GetData();
        auto buffer = _writeBuffer.prepare(1 + data.size());

        if (_cryptation && _cryptation->IsReady()) {
            _cryptation->Encrypt(static_cast<uint8_t *>(buffer.data()), &header,
                                 1);
            _cryptation->Encrypt(static_cast<uint8_t *>(buffer.data()) + 1,
                                 data.data(), data.size());
        } else {
            boost::asio::buffer_copy(boost::asio::buffer(buffer, 1),
                                     boost::asio::buffer(&header, 1));
            boost::asio::buffer_copy(
                boost::asio::buffer(buffer + 1, data.size()),
                boost::asio::buffer(data.data(), data.size()));
        }

        _writeBuffer.commit(buffer.size());
        _socket.async_write_some(
            buffer,
            boost::asio::bind_executor(
                _strand,
                boost::bind(&Connection::HandleWrite, shared_from_this(),
                            boost::asio::placeholders::error,
                            boost::asio::placeholders::bytes_transferred)));
    }

    void Connection::SendAsReply(std::shared_ptr<Packet> request,
                                 std::shared_ptr<Packet> reply) {
        auto id = request->GetField<uint64_t>("__REFERENCE_ID");

        reply->SetField<uint64_t>("__REFERENCE_ID", id);
        reply->SetField<uint8_t>("__REFERENCE_TYPE", 1);

        Send(reply);
    }

    void Connection::SetPhase(uint8_t phase) {
        CORE_LOGGING(trace)
            << "Set phase to " << boost::format("0x%02x") % (int)phase;

        auto packet = _server->GetPacketManager()->CreatePacket(
            ReservedOutgoingHeaders::HEADER_OUT_PHASE, Direction::Outgoing);
        packet->SetField<uint8_t>("phase", phase);

        _phase = phase;

        Send(packet);
    }

    bool Connection::HasProperty(const std::string &property) const {
        return _properties.find(property) != _properties.end();
    }

    void Connection::SetProperty(const std::string &property,
                                 ConnectionProperty value) {
        _properties[property] = std::move(value);
    }

    ConnectionProperty Connection::GetProperty(
        const std::string &property) const {
        return _properties.at(property);
    }

    void Connection::HandleHandshake(std::shared_ptr<Packet> packet) {
        auto handshake = packet->GetField<uint32_t>("handshake");
        auto time = packet->GetField<uint32_t>("time");
        auto delta = packet->GetField<uint32_t>("delta");

        if (handshake != _handshake) {
            CORE_LOGGING(warning)
                << "Closing connection because of handshake mismatch";
            Close();
            return;
        }

        auto currentTime = _server->GetApplication()->GetCoreTime();

        auto diff = currentTime - (time + delta);
        if (diff >= 0 && diff <= 50) {
            CORE_LOGGING(trace) << "Time sync done, handshake done";

            if (_securityLevel >= SECURITY_LEVEL_KEY_AGREEMENT) {
                StartKeyAgreement();
            } else {
                _handshaking = false;
                _server->CallNewConnectionHandler(shared_from_this());

                if (_securityLevel == SECURITY_LEVEL_XTEA) {
                    CORE_LOGGING(trace) << "(XTEA) Cryptation enabled";
                    if (!_cryptation->Finalize()) {
                        Close();
                    }

                    _cryptation->Activate();
                }
            }
            return;
        }

        auto newDelta = (currentTime - time) / 2;
        if (newDelta < 0) {
            CORE_LOGGING(error) << "Failed to sync time";
            Close();
            return;
        }

        // todo max retries?
        SendHandshake(currentTime, newDelta);
    }

    std::shared_ptr<Server> Connection::GetServer() { return _server; }

    void Connection::StartHandshake() {
        _handshaking = true;
        SetPhase(Phases::PHASE_HANDSHAKE);
        SendHandshake(_server->GetApplication()->GetCoreTime(), 0);
    }

    void Connection::SendHandshake(uint32_t time, uint32_t delta) {
        auto packet = _server->GetPacketManager()->CreatePacket(
            ReservedOutgoingHeaders::HEADER_OUT_HANDSHAKE, Direction::Outgoing);
        packet->SetField<uint32_t>("handshake", _handshake);
        packet->SetField<uint32_t>("time", time);
        packet->SetField<uint32_t>("delta", 0);

        Send(packet);
    }

    void Connection::ReadNextPacket() {
        CORE_LOGGING(trace) << "Reading next packet";
        boost::asio::async_read(
            _socket, _buffer.prepare(1), boost::asio::transfer_exactly(1),
            boost::asio::bind_executor(
                _strand,
                boost::bind(&Connection::HandleReadHeader, shared_from_this(),
                            boost::asio::placeholders::error,
                            boost::asio::placeholders::bytes_transferred)));
    }

    void Connection::HandleReadHeader(const boost::system::error_code &err,
                                      std::size_t transferred) {
        _buffer.commit(transferred);
        if (!err) {
            CORE_LOGGING(trace)
                << "(HEADER) Received " << transferred << " bytes";

            uint8_t header;
            boost::asio::buffer_copy(
                boost::asio::buffer(&header, sizeof(header)), _buffer.data());

            if (_cryptation) {
                if (_phase > PHASE_HANDSHAKE &&
                    !_cryptation->IsReady()) {
                    // workaround for broken client implementation
                    _cryptation->Activate();
                }

                if (_cryptation->IsReady()) {
                    _cryptation->Decrypt(
                        &header,
                        reinterpret_cast<const uint8_t *>(
                            boost::asio::buffer(_buffer.data(), sizeof(header))
                                .data()),
                        1);
                }
            }

            _buffer.consume(1);

            CORE_LOGGING(trace)
                << "Received header " << boost::format("0x%02x") % (int)header;

            auto packet =
                _server->GetPacketManager()->CreatePacket(header, Incoming);
            if (!packet) {
                CORE_LOGGING(warning) << "Received unknown header "
                                      << boost::format("0x%02x") % (int)header;
                Close();
                return;
            }

            // Check if packet is dynamic size
            if (packet->IsDynamicSized()) {
                CORE_LOGGING(trace) << "Reading dynamic packet size";

                // We have to read our packet size first
                boost::asio::async_read(
                    _socket, _buffer.prepare(2),
                    boost::asio::transfer_exactly(2),
                    boost::asio::bind_executor(
                        _strand,
                        boost::bind(
                            &Connection::HandleReadSize, shared_from_this(),
                            packet, boost::asio::placeholders::error,
                            boost::asio::placeholders::bytes_transferred)));

                return;
            }

            auto sizeToRead = packet->GetSize();
            if (packet->HasSequence()) sizeToRead += 1;
            CORE_LOGGING(trace)
                << "Reading the next " << sizeToRead << " bytes";

            boost::asio::async_read(
                _socket, _buffer.prepare(sizeToRead),
                boost::asio::transfer_exactly(sizeToRead),
                boost::asio::bind_executor(
                    _strand,
                    boost::bind(&Connection::HandleReadData, shared_from_this(),
                                packet, boost::asio::placeholders::error,
                                boost::asio::placeholders::bytes_transferred)));
        } else {
            if (err == boost::asio::error::eof) {
                CORE_LOGGING(debug)
                    << "Connection closed by remote "
                    << _socket.remote_endpoint().address().to_string();
            } else {
                CORE_LOGGING(warning) << "Failed to read " << err.message();
            }
            Close();
        }
    }

    void Connection::HandleReadSize(std::shared_ptr<Packet> packet,
                                    const boost::system::error_code &err,
                                    std::size_t transferred) {
        assert(transferred == 2);
        CORE_LOGGING(trace) << "(SIZE) Received " << transferred << " bytes";
        _buffer.commit(transferred);
        if (!err) {
            unsigned short size;

            if (_cryptation && _cryptation->IsReady()) {
                std::vector<uint8_t> buf(transferred);

                _cryptation->Decrypt(
                    buf.data(),
                    reinterpret_cast<const uint8_t *>(_buffer.data().data()),
                    transferred);
                _buffer.consume(transferred);

                size = (buf[1] << 8) | buf[0];
            } else {
                boost::asio::buffer_copy(
                    boost::asio::buffer(static_cast<void *>(&size), 2),
                    _buffer.data());  // todo: clean solution
            }

            CORE_LOGGING(trace) << "Size of packet is " << size;

            auto sizeToRead =
                size - 1 - 2;  // already read header (1byte) and size (2bytes)
            if (packet->HasSequence()) {
                sizeToRead += 1;
            }

            CORE_LOGGING(trace)
                << "Reading the next " << sizeToRead << " bytes";
            boost::asio::async_read(
                _socket, _buffer.prepare(sizeToRead),
                boost::asio::transfer_exactly(sizeToRead),
                boost::asio::bind_executor(
                    _strand,
                    boost::bind(&Connection::HandleReadData, shared_from_this(),
                                packet, boost::asio::placeholders::error,
                                boost::asio::placeholders::bytes_transferred)));
        } else {
            if (err == boost::asio::error::eof) {
                CORE_LOGGING(debug)
                    << "Connection closed by remote "
                    << _socket.remote_endpoint().address().to_string();
            } else {
                CORE_LOGGING(warning) << "Failed to read " << err.message();
            }
            Close();
        }
    }

    void Connection::HandleReadData(std::shared_ptr<Packet> packet,
                                    const boost::system::error_code &err,
                                    std::size_t transferred) {
        CORE_LOGGING(trace) << "(DATA) Received " << transferred << " bytes";
        _buffer.commit(transferred);
        if (!err) {
            if (_cryptation && _cryptation->IsReady()) {
                std::vector<uint8_t> buf(transferred);

                _cryptation->Decrypt(
                    buf.data(),
                    reinterpret_cast<const uint8_t *>(_buffer.data().data()),
                    transferred);
                _buffer.consume(transferred);

                packet->CopyData(buf, packet->IsDynamicSized() ? 2 : 0);
            } else {
                packet->CopyData(_buffer);
            }

#ifdef NDEBUG
            try {
#endif
                {
                    PROFILE_SCOPE("Handler for " + packet->GetName());
                    _server->CallPacketHandler(shared_from_this(), packet);
                }
#ifdef NDEBUG
            } catch (...) {
                CORE_LOGGING(fatal)
                    << boost::current_exception_diagnostic_information();
            }
#endif

            ReadNextPacket();
        } else {
            if (err == boost::asio::error::eof) {
                CORE_LOGGING(debug)
                    << "Connection closed by remote "
                    << _socket.remote_endpoint().address().to_string();
            } else {
                CORE_LOGGING(warning) << "Failed to read " << err.message();
            }
            Close();
        }
    }

    void Connection::HandleWrite(const boost::system::error_code &err,
                                 std::size_t transferred) {
        _writeBuffer.consume(transferred);
        if (err) {
            CORE_LOGGING(warning)
                << "Failed to send data to "
                << _socket.remote_endpoint().address().to_string() << ": "
                << err.message();
            Close();
        } else {
            {
                std::lock_guard<std::mutex> guard(_sendMutex);
                if (_sendQueue.empty()) return;
                _sendQueue.pop();
                if (_sendQueue.empty()) return;
            }
            HandleSend();
        }
    }

    boost::asio::ip::tcp::socket &Connection::GetSocket() { return _socket; }

    std::shared_ptr<Connection> Connection::Create(
        boost::asio::io_context &context, std::shared_ptr<Server> server,
        uint8_t securityLevel, uint32_t *defaultXTEAKeys) {
        return std::make_shared<Connection>(context, server, securityLevel,
                                            defaultXTEAKeys);
    }

    void Connection::StartKeyAgreement() {
        CORE_LOGGING(trace) << "(KEYS) Agreement start!";
        const CryptoPP::SecByteBlock *staticKey;
        const CryptoPP::SecByteBlock *ephemeralKey;
        const uint32_t *agreedValue;

        if (!_cryptation->Initialize()) {
            CORE_LOGGING(trace) << "(KEYS) Cannot initialize!";
            Close();
            return;
        }

        staticKey = (const CryptoPP::SecByteBlock *)(_cryptation->GetData(
            KEY_AGREEMENT_DATA_PUBLIC_STATIC_SERVER_KEY));
        ephemeralKey = (const CryptoPP::SecByteBlock *)(_cryptation->GetData(
            KEY_AGREEMENT_DATA_PUBLIC_EPHEMERAL_SERVER_KEY));

        agreedValue = (const uint32_t *)_cryptation->GetData(
            KEY_AGREEMENT_DATA_AGREED_VALUE);
        if (*agreedValue < 1 || staticKey->size() < 1 ||
            ephemeralKey->size() < 1 ||
            (ephemeralKey->size() + staticKey->size() > 256)) {
            CORE_LOGGING(trace) << "(KEYS) Invalid values!";
            Close();
            return;
        }

        char keys[256];
        memset(keys, 0, sizeof(keys));
        memcpy(keys, staticKey->data(), staticKey->size());
        memcpy(keys + staticKey->size(), ephemeralKey->data(),
               ephemeralKey->size());

        auto packet = _server->GetPacketManager()->CreatePacket(
            ReservedOutgoingHeaders::HEADER_OUT_KEY_AGREEMENT,
            Direction::Outgoing);
        packet->SetField<uint16_t>("valuelen",
                                   static_cast<uint16_t>(*agreedValue));
        packet->SetField<uint16_t>(
            "datalen",
            static_cast<uint16_t>(ephemeralKey->size() + staticKey->size()));
        packet->SetField("data", (uint8_t *)keys, sizeof(keys));

        Send(packet);
    }

    void Connection::HandleKeyAgreement(std::shared_ptr<Packet> packet) {
        PROFILE_FUNCTION();
        if (!_cryptation->IsInitialized()) {
            CORE_LOGGING(trace) << "(KEYS) Connection sent handle keys without "
                                   "letting the server initialize them";
            Close();
            return;
        }

        uint8_t *data = (uint8_t *)packet->GetField("data");

        {
            PROFILE_SCOPE("Generate keys");
            const CryptoPP::SecByteBlock *staticKey =
                (const CryptoPP::SecByteBlock *)_cryptation->GetData(
                    KEY_AGREEMENT_DATA_PUBLIC_STATIC_SERVER_KEY);
            const CryptoPP::SecByteBlock *ephemeralKey =
                (const CryptoPP::SecByteBlock *)_cryptation->GetData(
                    KEY_AGREEMENT_DATA_PUBLIC_EPHEMERAL_SERVER_KEY);

            uint16_t agreedValue = packet->GetField<uint16_t>("valuelen");
            _cryptation->AddData(KEY_AGREEMENT_DATA_AGREED_VALUE, &agreedValue,
                                 sizeof(agreedValue));

            _cryptation->AddData(KEY_AGREEMENT_DATA_PUBLIC_STATIC_CLIENT_KEY,
                                 data, staticKey->size());
            _cryptation->AddData(KEY_AGREEMENT_DATA_PUBLIC_EPHEMERAL_CLIENT_KEY,
                                 data + staticKey->size(),
                                 ephemeralKey->size());
        }

        if (!_cryptation->Finalize()) {
            CORE_LOGGING(trace) << "(KEYS) Cannot agree";
            Close();
            return;
        }

        Send(_server->GetPacketManager()->CreatePacket(
            ReservedOutgoingHeaders::HEADER_OUT_KEY_AGREEMENT_COMPLETED,
            Direction::Outgoing));

        CORE_LOGGING(trace) << "(KEYS) Agreement completed!";
        _handshaking = false;
        _server->CallNewConnectionHandler(shared_from_this());
    }

    void Connection::ChangeXTEAKey(uint32_t *key) {
        _cryptation->AddData(XTEA_CRYPTATION_LOGIN_DECRYPT_KEY, key, 16);
    }
}  // namespace core::networking
