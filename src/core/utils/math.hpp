// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#include <cmath>

namespace core::utils {
    class Math {
       public:
        static float Distance(float x1, float y1, float x2, float y2) {
            auto a = x1 - x2;
            auto b = y1 - y2;

            return sqrt(a * a + b * b);
        }

        static float DotProduct(float x1, float y1, float x2, float y2) {
            return x1 * x2 + y1 * y2;
        }

        static float CalculateRotation(float x, float y) {
            float vectorLength = sqrtf(x * x + y * y);

            float normalizedX = x / vectorLength;
            float normalizedY = y / vectorLength;
            float upVectorX = 0;
            float upVectorY = 1;

            float rotationRadians = acosf(
                DotProduct(normalizedX, normalizedY, upVectorX, upVectorY));
            return rotationRadians * (180 / 3.14159265358979323846);
        }
    };
}  // namespace core::utils