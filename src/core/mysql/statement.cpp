// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "statement.hpp"

#include <mysql.h>
#include <utility>

#include "mysql.hpp"

namespace core::mysql {
    Statement::Statement(std::shared_ptr<MySQL> mysql,
                         const std::string &statement)
        : _mysql(std::move(mysql)),
          _statement(statement),
          _formatter(statement) {}

    std::string Statement::EscapeString(const std::string &str) {
        auto buffer = new char
            [str.size() * 2 +
             1];  // buffer size is recommended
                  // (https://dev.mysql.com/doc/refman/8.0/en/mysql-real-escape-string.html)
        auto len = mysql_real_escape_string(_mysql->_mysql, buffer, str.c_str(),
                                            str.size());
        auto ret = std::string(buffer, len);
        delete[] buffer;

        return ret;
    }

    std::shared_ptr<ResultSet> Statement::operator()() {
        return _mysql->ExecuteQuery(GetStatement());
    }

    std::string Statement::GetStatement() { return _formatter.str(); }

    Statement &operator<<(Statement &stmt, const std::string &value) {
        stmt._formatter % ("'" + stmt.EscapeString(value) + "'");
        return stmt;
    }

    Statement &operator<<(Statement &stmt, int value) {
        stmt._formatter % value;
        return stmt;
    }
}  // namespace core::mysql