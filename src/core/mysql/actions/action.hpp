// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#include <string>

namespace core::mysql::actions {
    class Action {
       public:
        virtual ~Action() = default;

        virtual std::string ToSQL() = 0;
    };
}  // namespace core::mysql::actions
