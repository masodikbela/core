// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "migration.hpp"

#include "../logger.hpp"
#include "actions/action.hpp"

namespace core::mysql {
    Migration::Migration(const std::string &name) : _name(name) {}

    Migration::~Migration() {
        for (auto action : _actions) {
            delete action;
        }
    }

    const std::string &Migration::GetName() const { return _name; }

    void Migration::Add(actions::Action *action) { _actions.push_back(action); }

    bool Migration::Execute(std::shared_ptr<MySQL> mysql) {
        mysql->DisableAutocommit();

        for (auto &action : _actions) {
            auto sql = action->ToSQL();
            if (!mysql->Execute(sql)) {
                CORE_LOGGING(fatal) << "Failed to migrate " << _name;
                CORE_LOGGING(fatal) << sql;
                CORE_LOGGING(fatal) << "Error: " << mysql->GetError();

                mysql->Rollback();
                mysql->EnableAutocommit();

                return false;
            }
        }

        mysql->Commit();
        mysql->EnableAutocommit();

        return true;
    }
}  // namespace core::mysql