// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "mysql.hpp"

#include "../logger.hpp"

namespace core::mysql {
    MySQL::MySQL(const std::string &host, const std::string &username,
                 const std::string &password, unsigned int port) {
        _mysql = mysql_init(nullptr);
        if (mysql_real_connect(_mysql, host.c_str(), username.c_str(),
                               password.c_str(), nullptr, port, nullptr, 0)) {
            CORE_LOGGING(info) << "Database connection to " << host << ":"
                               << port << " established";
        } else {
            CORE_LOGGING(error)
                << "Failed to connect to database at " << host << ":" << port;
            CORE_LOGGING(error) << mysql_error(_mysql);
        }
    }

    MySQL::~MySQL() { mysql_close(_mysql); }

    std::string MySQL::GetError() { return std::string(mysql_error(_mysql)); }

    void MySQL::EnableAutocommit() { mysql_autocommit(_mysql, true); }

    void MySQL::DisableAutocommit() { mysql_autocommit(_mysql, false); }

    bool MySQL::Commit() { return mysql_commit(_mysql); }

    bool MySQL::Rollback() { return mysql_rollback(_mysql); }

    Statement MySQL::CreateStatement(const std::string &statement) {
        return Statement(shared_from_this(), statement);
    }

    std::shared_ptr<ResultSet> MySQL::ExecuteQuery(
        const std::string &statement) {
        mysql_real_query(_mysql, statement.c_str(), statement.size());
        return std::make_shared<ResultSet>(shared_from_this());
    }

    bool MySQL::Execute(const std::string &statement) {
        return mysql_real_query(_mysql, statement.c_str(), statement.size()) ==
               0;
    }

    std::string MySQL::ExecuteScalar(std::string statement) {
        if (!Execute(statement)) throw std::runtime_error(GetError());

        auto result = mysql_store_result(_mysql);
        if (mysql_num_rows(result) != 1) {
            mysql_free_result(result);
            throw std::runtime_error("Expected result size of 1");
        }

        auto row = mysql_fetch_row(result);
        std::string str(row[0]);

        mysql_free_result(result);

        return str;
    }
}  // namespace core::mysql