// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#include <mysql.h>
#include <string>

#include "result_set.hpp"
#include "statement.hpp"

namespace core::mysql {
    class MigrationManager;

    class MySQL : public std::enable_shared_from_this<MySQL> {
        friend class ResultSet;

        friend class Statement;

       public:
        MySQL(const std::string &host, const std::string &username,
              const std::string &password, unsigned int port);
        virtual ~MySQL();

        std::string GetError();

        void EnableAutocommit();
        void DisableAutocommit();
        bool Commit();
        bool Rollback();

        Statement CreateStatement(const std::string &statement);
        std::shared_ptr<ResultSet> ExecuteQuery(const std::string &statement);
        std::shared_ptr<ResultSet> ExecuteQuery(const Statement &statement);

        bool Execute(const std::string &statement);
        std::string ExecuteScalar(std::string statement);

       protected:
        MYSQL *_mysql;
    };
}  // namespace core::mysql