// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

namespace core {
    class ApplicationAbstract {
       public:
        virtual void Start() = 0;
        virtual void Update(uint32_t elapsedTime) = 0;
    };
}  // namespace core
