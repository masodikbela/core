// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "logger.hpp"

namespace core {
    std::string path_to_filename(std::string path) {
        return path.substr(path.find_last_of("/\\") + 1);
        // return path.substr(0, path.find_last_of("."));
    }
}  // namespace core
