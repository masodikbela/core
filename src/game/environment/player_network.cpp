// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "../application.hpp"
#include "../commands/command_manager.hpp"
#include "player.hpp"

namespace game::environment {
    void Player::OnItemMove(
        const std::shared_ptr<core::networking::Packet> &packet) {
        auto fromWindow = packet->GetField<uint8_t>("fromWindow");
        auto fromPosition = packet->GetField<uint16_t>("fromPosition");
        auto toWindow = packet->GetField<uint8_t>("toWindow");
        auto toPosition = packet->GetField<uint16_t>("toPosition");

        if (fromWindow == 1 && fromPosition >= 90) {
            fromWindow = 2;
            fromPosition -= 90;
        }
        if (toWindow == 1 && toPosition >= 90) {
            toWindow = 2;
            toPosition -= 90;
        }

        CORE_LOGGING(trace)
            << "Move from " << static_cast<uint16_t>(fromWindow) << ","
            << fromPosition << " to " << static_cast<uint16_t>(toWindow) << ","
            << toPosition;

        std::shared_ptr<item::Item> item = GetItem(fromWindow, fromPosition);

        if (!HasSpace(toWindow, toPosition, item->GetProto().size)) {
            return;
        }
        if (!RemoveItem(item)) {
            CORE_LOGGING(error) << "Failed to remove item!";
            return;
        }
        if (!SetItem(toWindow, toPosition, item)) {
            CORE_LOGGING(error) << "Failed to set item!";
            SetItem(fromWindow, fromPosition,
                    item);  // set item back to original position!
            return;
        }
        item->Persist();

        if (fromWindow != toWindow) {
            auto itemManager =
                game::Application::GetInstance()->GetItemManager();
            itemManager->RemoveFromWindow(fromWindow, _id, item->GetId());
            itemManager->AddToWindow(toWindow, _id, item->GetId());
        }
    }

    void Player::OnChat(
        const std::shared_ptr<core::networking::Packet> &packet) {
        auto messageType = packet->GetField<uint8_t>("messageType");
        auto message = packet->GetDynamicString();

        if (message.empty()) return;

        if (message[0] == '/') {
            // Received command
            commands::CommandManager::Execute(shared_from_this(), message);
            return;
        }

        switch (messageType) {
            case ChatTypes::CHAT_NORMAL: {
                auto chat =
                    _connection->GetServer()->GetPacketManager()->CreatePacket(
                        0x04, core::networking::Direction::Outgoing);
                chat->SetField<uint8_t>("messageType", ChatTypes::CHAT_NORMAL);
                chat->SetField<uint32_t>("vid", GetVID());
                chat->SetField<uint8_t>("empire", 1);

                message = GetName() + " : " + message;
                chat->SetDynamicString(message);

                _connection->Send(chat);
                SendPacketAround(chat);
                break;
            }
            case ChatTypes::CHAT_GROUP:
                break;
            case ChatTypes::CHAT_GUILD:
                break;
            case ChatTypes::CHAT_SHOUT: {
                auto chat =
                    _connection->GetServer()->GetPacketManager()->CreatePacket(
                        0x04, core::networking::Direction::Outgoing);
                chat->SetField<uint8_t>("messageType", ChatTypes::CHAT_SHOUT);
                chat->SetField<uint32_t>("vid", 0);
                chat->SetField<uint8_t>("empire", 1);

                message = GetName() + " : " + message;
                chat->SetDynamicString(message);

                _connection->GetServer()->SendToAll(chat);
                break;
            }
            default:
                CORE_LOGGING(error)
                    << "Unknown chat type " << static_cast<int>(messageType);
                _connection->PostClose();
                break;
        }
    }

    void Player::OnMove(
        const std::shared_ptr<core::networking::Packet> &packet) {
        auto movementType = packet->GetField<uint8_t>("movementType");
        if (movementType >= MovementTypes::MAX &&
            movementType != MovementTypes::SKILL) {
            CORE_LOGGING(error) << "Received unknown movement type";
            _connection->PostClose();
            return;
        }

        auto rotation = packet->GetField<uint8_t>("rotation");
        auto x = packet->GetField<int32_t>("x");
        auto y = packet->GetField<int32_t>("y");

        // todo: check if position is possible

        CORE_LOGGING(trace) << "Received move packet with type "
                            << static_cast<uint16_t>(movementType);
        if (movementType == MovementTypes::MOVE) {
            SetRotation(rotation * 5);
            Goto(x, y);
        }

        // Forward move packet to all players around
        auto move = _connection->GetServer()->GetPacketManager()->CreatePacket(
            0x03, core::networking::Direction::Outgoing);
        move->SetField("movementType", movementType);
        move->SetField("argument", packet->GetField<uint8_t>("argument"));
        move->SetField("rotation", packet->GetField<uint8_t>("rotation"));
        move->SetField("vid", _vid);
        move->SetField("x", x);
        move->SetField("y", y);
        move->SetField("time", packet->GetField<uint32_t>("time"));
        move->SetField("duration", movementType == MovementTypes::MOVE
                                       ? _movementDuration
                                       : 0);
        SendPacketAround(move);
    }
}  // namespace game::environment