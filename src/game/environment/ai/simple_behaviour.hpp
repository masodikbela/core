// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
#pragma once

#include <memory>

#include "../monster.hpp"
#include "behaviour.hpp"

namespace game::environment::ai {
    enum State { IDLE, MOVING };

    class SimpleBehaviour : public Behaviour {
       public:
        explicit SimpleBehaviour(std::shared_ptr<Monster> &&monster);
        virtual ~SimpleBehaviour();

        void Init() override;
        void Update(uint32_t elapsedTime) override;
        void ObjectEnteredView(const std::shared_ptr<Object> &object) override;

       private:
        void CalculateNextMovement();
        bool RandomTarget();
        void SendMovePacket();

       private:
        std::shared_ptr<Monster> _monster;
        State _state = IDLE;

        int32_t _startX;
        int32_t _startY;
        int32_t _targetX;
        int32_t _targetY;
        uint32_t _movementStart;
        uint32_t _movementDuration;

        int _nextMovementIn;
    };
}  // namespace game::environment::ai
