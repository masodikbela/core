// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
#pragma once

namespace game::environment {
    class Object;
}

namespace game::environment::ai {
    class Behaviour {
       public:
        virtual ~Behaviour() = default;

        virtual void Init() = 0;
        virtual void Update(uint32_t elapsedTime) = 0;

        virtual void ObjectEnteredView(
            const std::shared_ptr<Object> &object) = 0;
    };
}  // namespace game::environment::ai