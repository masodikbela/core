// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#include <cpptoml.h>
#include <map>
#include <memory>
#include <mutex>
#include <queue>
#include <string>
#include <unordered_map>

#include "../formats/spawn_groups.hpp"
#include "object.hpp"
#include "quad_tree.hpp"

#define UNIT_SIZE 25600

namespace game::environment {
    class World;
    class Monster;
    class Map;

    class SpawnData {
       public:
        enum SpawnType { SPAWN_GROUP, SPAWN_MONSTER };

       public:
        SpawnData();
        virtual ~SpawnData();

        void Load(const std::shared_ptr<cpptoml::table> &config);
        void Spawn(const std::shared_ptr<Map> &map);

       private:
        SpawnType _type;
        int32_t _x;
        int32_t _y;
        uint32_t _range;
        uint64_t _respawnTime;

        std::vector<uint32_t> _monsters;
        std::vector<uint64_t> _groups;

        std::unordered_map<uint32_t, std::shared_ptr<Monster>> _instances;
    };

    class Map : public std::enable_shared_from_this<Map> {
        friend SpawnData;

       public:
        Map(std::string name, uint64_t x, uint64_t y, uint8_t width,
            uint8_t height, std::shared_ptr<World> world);
        virtual ~Map();

        bool Load();
        void Update(uint32_t elapsedTime);

        void InitialSpawn();

        [[nodiscard]] std::string GetName() const { return _name; }
        [[nodiscard]] uint64_t GetX() const { return _x; }
        [[nodiscard]] uint64_t GetUnitX() const { return _x / UNIT_SIZE; }
        [[nodiscard]] uint64_t GetY() const { return _y; }
        [[nodiscard]] uint64_t GetUnitY() const { return _y / UNIT_SIZE; }
        [[nodiscard]] uint8_t GetWidth() const { return _width; }
        [[nodiscard]] uint8_t GetHeight() const { return _height; }

        void SpawnObject(const std::shared_ptr<Object> &object);
        void DespawnObject(const std::shared_ptr<Object> &object);
        void QueryObjectsAround(std::vector<std::shared_ptr<Object>> &out,
                                uint64_t x, uint64_t y, uint32_t radius);

       private:
        std::string _name;
        uint64_t _x;
        uint64_t _y;
        uint8_t _width;
        uint8_t _height;

        std::shared_ptr<World> _world;

        std::vector<SpawnData> _spawnData;

        std::map<uint32_t, std::shared_ptr<Object>> _objects;
        QuadTree _quadTree;

        std::queue<std::shared_ptr<Object>> _despawnQueue;
        std::mutex _despawnMutex;
        std::queue<std::shared_ptr<Object>> _spawnQueue;
        std::mutex _spawnMutex;
    };
}  // namespace game::environment