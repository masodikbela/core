// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "item_manager.hpp"

#include "../../core/logger.hpp"
#include "../../core/mysql/migrations/_defaults.hpp"
#include "../../core/profiler.hpp"
#include "item.hpp"

namespace game::item {
    ItemManager::ItemManager(std::shared_ptr<core::cache::Redis> redis,
                             std::shared_ptr<core::mysql::MySQL> database)
        : _protos(), _redis(std::move(redis)), _database(std::move(database)) {}

    ItemManager::~ItemManager() = default;

    void ItemManager::Load() {
        PROFILE_FUNCTION();
        formats::ItemProto proto;
        if (!proto.Load("item_proto", _protos)) {
            return;
        }

        CORE_LOGGING(info) << "Loaded " << _protos.size() << " items";
    }

    const formats::Item &ItemManager::GetProto(uint32_t id) const {
        return _protos.at(id);
    }

    bool ItemManager::HasProto(uint32_t id) const {
        return _protos.count(id) != 0;
    }

    std::shared_ptr<Item> ItemManager::CreateItem(uint32_t vnum,
                                                  uint32_t owner) {
        // Generate item id
        auto id = _redis->Execute<int64_t>(
            bredis::single_command_t{"incr", "item_counter"});
        auto item = std::make_shared<Item>(shared_from_this(), _protos[vnum],
                                           owner, id, 1);
        item->Persist(true);

        return item;
    }

    std::shared_ptr<Item> ItemManager::GetItemById(uint64_t id) {
        return Item::Load(shared_from_this(), id);
    }

    std::vector<std::shared_ptr<Item>> ItemManager::QueryItems(
        uint32_t playerId, uint16_t window) {
        std::vector<std::shared_ptr<Item>> ret;

        auto key =
            "items:" + std::to_string(playerId) + ":" + std::to_string(window);

        auto exists =
            _redis->Execute<int64_t>(bredis::single_command_t{"exists", key});
        if (exists == 0) {
            // Load data from database
            auto stmt = _database->CreateStatement(
                "SELECT id FROM `" GAME_DATABASE
                "`.`items` WHERE player_id = %1% and window = %2%");
            stmt << playerId << window;

            auto result = stmt();
            while (result->Next()) {
                // Load the item from the cache or from the database
                auto itemId = result->GetUnsigned64("id");
                CORE_LOGGING(trace) << "Query item found: " << itemId;
                ret.push_back(GetItemById(itemId));
                _redis->Execute(bredis::single_command_t{
                    "lpush", key, std::to_string(itemId)});
            }
        } else {
            auto length =
                _redis->Execute<int64_t>(bredis::single_command_t{"llen", key});
            for (auto i = 0; i < length; i++) {
                auto id = _redis->Execute<core::cache::string_t>(
                    bredis::single_command_t{"lindex", key, std::to_string(i)});
                ret.push_back(
                    GetItemById(boost::lexical_cast<int64_t>(id.str)));
            }
        }

        return ret;
    }

    void ItemManager::RemoveFromWindow(uint16_t window, uint32_t playerId,
                                       uint64_t id) {
        auto key =
            "items:" + std::to_string(playerId) + ":" + std::to_string(window);
        _redis->Execute(
            bredis::single_command_t{"lrem", key, "1", std::to_string(id)});
    }
    void ItemManager::AddToWindow(uint16_t window, uint32_t playerId,
                                  uint64_t id) {
        auto key =
            "items:" + std::to_string(playerId) + ":" + std::to_string(window);
        _redis->Execute(
            bredis::single_command_t{"rpush", key, std::to_string(id)});
    }
}  // namespace game::item