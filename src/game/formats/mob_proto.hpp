// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#include <map>
#include <string>

namespace game::formats {
    struct MonsterSkill {
        uint32_t id;
        uint8_t level;
    };

    struct Monster {
        uint32_t id;
        std::string name;
        std::string translatedName;
        uint8_t type;
        uint8_t rank;
        uint8_t battleType;
        uint8_t level;
        uint8_t size;
        uint32_t minGold;
        uint32_t maxGold;
        uint32_t experience;
        uint32_t hp;
        uint8_t regenDelay;
        uint8_t regenPercentage;
        uint16_t defence;
        uint32_t aiFlag;
        uint32_t raceFlag;
        uint32_t immuneFlag;
        uint8_t str;
        uint8_t dex;
        uint8_t con;
        uint8_t inte;
        uint32_t damageRange[2];
        int16_t attackSpeed;
        int16_t moveSpeed;
        uint8_t aggressivePct;
        uint16_t aggressiveSight;
        uint16_t attackRange;
        uint8_t enchantments[6];
        uint8_t resists[11];
        uint32_t resurrectionId;
        uint32_t dropItemId;
        uint8_t mountCapacity;
        uint8_t onClickType;
        uint8_t empire;
        std::string folder;
        float damageMultiply;
        uint32_t summonId;
        uint32_t drainSP;
        uint32_t monsterColor;
        uint32_t polymorphItemId;
        MonsterSkill skills[5];
        uint8_t berserkPoint;
        uint8_t stoneSkinPoint;
        uint8_t godSpeedPoint;
        uint8_t deathBlowPoint;
        uint8_t revivePoint;

        void Read(uint8_t *ptr);
    };

    class MobProto {
    public:
        static bool Load(const std::string &file, std::map<uint32_t, Monster> &monsters);
    };
}