// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "crypted_file.hpp"

#include "../../core/logger.hpp"
#include "../../core/utils/xtea.hpp"

#include <lzokay.hpp>

namespace game::formats {
    CryptedFile::CryptedFile(const std::vector<uint8_t> &data,
                             const uint32_t *key, unsigned int offset) {
        // Read header of crypted block
        // The header contains of a four cc, the encrypted size, the compressed
        // size and the decompressed size
        auto position = data.begin() + offset;
        _fourCC = std::string(position, position + 4);
        position += 4;

        _size = (*position) | ((*(position + 1)) << 8) |
                ((*(position + 2)) << 16) | ((*(position + 3)) << 24);
        position += 4;

        _compressSize = (*position) | ((*(position + 1)) << 8) |
                        ((*(position + 2)) << 16) | ((*(position + 3)) << 24);
        position += 4;

        _realSize = (*position) | ((*(position + 1)) << 8) |
                    ((*(position + 2)) << 16) | ((*(position + 3)) << 24);
        position += 4;

        CORE_LOGGING(trace) << _fourCC << " size: " << _size
                            << " compressed_size: " << _compressSize
                            << " actual_size: " << _realSize;

        // After the header the crypted blocks start
        // The block starts with a four cc again
        std::vector<uint8_t> decrypted;
        decrypted.resize(_size);
        core::utils::XTEA::Decrypt(&*position, &decrypted[0], _size, &key[0],
                                   32);
        core::utils::XTEA::Decrypt(&*position, &decrypted[0], _size, key, 32);

        // Verify the four cc so we can make sure that our decryption worked
        auto fourCC = reinterpret_cast<const char *>(&decrypted[0]);
        auto cmp = strncmp(fourCC, "MCOZ", 4);
        if (cmp != 0) {
            CORE_LOGGING(error)
                << "Failed to read crypted file, four cc mismatch!";
            return;
        }

        CORE_LOGGING(trace) << "decrypted";

        // The data is also lzo1x compressed, we prepare our internal buffer and
        // than uncompress the data The four cc from earlier is not part of lzo
        _data.resize(_realSize);
        size_t decompressedSize = 0;
        auto result = lzokay::decompress(
            &decrypted[4], _compressSize, &_data[0], _realSize,
            decompressedSize);

        if (result < lzokay::EResult::Success) {
            CORE_LOGGING(error)
                << "Failed to decompress data! (err " << static_cast<int32_t>(result) << ")";
            return;
        }

        CORE_LOGGING(trace) << decompressedSize << " decompressed";
    }

    CryptedFile::CryptedFile(const std::string &file, const uint32_t *key,
                             unsigned int offset) {}

    const std::vector<uint8_t> &CryptedFile::GetData() { return _data; }

    CryptedFile::~CryptedFile() = default;
}  // namespace game::formats
