// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "command_manager.hpp"

#include <sstream>
#include <utility>

#include "../../core/logger.hpp"
#include "../../core/networking/reserved_headers.hpp"
#include "../application.hpp"
#include "../environment/player.hpp"

namespace game::commands {
    std::vector<Command> CommandManager::_commands;

    void CommandManager::RegisterCommand(const std::string &name,
                                         CommandFunction &&func) {
        Command command{name, std::move(func)};
        _commands.push_back(command);
    }

    void CommandManager::Execute(
        const std::shared_ptr<environment::Player> &player,
        const std::string &command) {
        if (!player) return;

        std::stringstream stream(command.substr(1));
        std::string segment;
        std::vector<std::string> arguments;

        while (std::getline(stream, segment, ' ')) {
            arguments.push_back(segment);
        }

        auto commandName = arguments.front();
        for (const auto &command : _commands) {
            if (command.name == commandName) {
                command.func(player, arguments);
                return;
            }
        }

        player->SendChatMessage(game::environment::CHAT_INFO,
                                "Command not found.");
    }

    void CommandManager::RegisterDefaultCommands() {
        RegisterCommand(
            "item", [](const std::shared_ptr<environment::Player> &player,
                       const std::vector<std::string> &arguments) {
                if (arguments.size() != 2) {
                    player->SendChatMessage(game::environment::CHAT_INFO,
                                            "/item [vnum]");
                    return;
                }

                try {
                    auto str = arguments[1];
                    auto vnum = boost::lexical_cast<uint32_t>(arguments[1]);
                    auto itemManager =
                        game::Application::GetInstance()->GetItemManager();
                    if (!itemManager->HasProto(vnum)) {
                        player->SendChatMessage(game::environment::CHAT_INFO,
                                                "Item not found.");
                        return;
                    }

                    auto proto = itemManager->GetProto(vnum);
                    if (!player->HasSpace(proto.size)) {
                        player->SendChatMessage(game::environment::CHAT_INFO,
                                                "No space in inventory.");
                        return;
                    }

                    auto item = itemManager->CreateItem(vnum, player->GetID());
                    player->GiveItem(item);
                } catch (boost::bad_lexical_cast &e) {
                    CORE_LOGGING(trace) << "'" << arguments[1] << "'";
                    player->SendChatMessage(game::environment::CHAT_INFO,
                                            "Invalid vnum.");
                }
            });

        RegisterCommand(
            "debug", [](const std::shared_ptr<environment::Player> &player,
                        const std::vector<std::string> &arguments) {
                if (arguments.size() < 2) {
                    player->SendChatMessage(game::environment::CHAT_INFO,
                                            "/debug [view|position]");
                    return;
                }

                if (arguments[1] == "view") {
                    player->DebugView();
                }

                if (arguments[1] == "position") {
                    player->SendChatMessage(
                        game::environment::CHAT_INFO,
                        "Global Position: " +
                            std::to_string(player->GetPositionX()) + ", " +
                            std::to_string(player->GetPositionY()));

                    auto localX =
                        player->GetPositionX() - player->GetMap()->GetX();
                    auto localY =
                        player->GetPositionY() - player->GetMap()->GetY();
                    player->SendChatMessage(
                        game::environment::CHAT_INFO,
                        "Global Position: " + std::to_string(localX) + ", " +
                            std::to_string(localY));
                }
            });

        RegisterCommand("quit",
                        [](const std::shared_ptr<environment::Player> &player,
                           const std::vector<std::string> &arguments) {
                            player->SendChatMessage(
                                game::environment::CHAT_COMMAND, "quit");
                            player->GetConnection()->PostShutdown();
                        });

        RegisterCommand("logout",
                        [](const std::shared_ptr<environment::Player> &player,
                           const std::vector<std::string> &arguments) {
                            player->GetConnection()->PostShutdown();
                        });

        RegisterCommand(
            "phase_select",
            [](const std::shared_ptr<environment::Player> &player,
               const std::vector<std::string> &arguments) {
                auto connection = player->GetConnection();

                connection->SetPlayer(nullptr);

                player->SetConnection(nullptr);
                auto map = player->GetMap();
                if (map) {
                    map->DespawnObject(player);
                }

                connection->SetPhase(core::networking::Phases::PHASE_SELECT);
            });

        RegisterCommand(
            "spawn", [](const std::shared_ptr<environment::Player> &player,
                        const std::vector<std::string> &arguments) {
                auto game = Application::GetInstance();
                auto core = game->GetCoreApplication();
                auto world = game->GetWorld();

                if (arguments.size() < 2) {
                    player->SendChatMessage(game::environment::CHAT_INFO,
                                            "/spawn vnum [count]");
                    return;
                }

                auto vnum = boost::lexical_cast<uint32_t>(arguments[1]);
                uint16_t count = 1;
                if (arguments.size() == 3) {
                    count = boost::lexical_cast<uint16_t>(arguments[2]);
                    if (count > 200) {
                        count = 200;
                    }
                }

                for (uint16_t i = 0; i < count; i++) {
                    auto offsetX = core->GetRandomNumber<short>(-1000, 1000);
                    auto offsetY = core->GetRandomNumber<short>(-1000, 1000);
                    auto rotation = core->GetRandomNumber<short>(0, 360);

                    auto monster = world->CreateMonster(
                        vnum, player->GetPositionX() + offsetX,
                        player->GetPositionY() + offsetY, rotation);
                    world->SpawnObject(monster);
                }
            });
    }
}  // namespace game::commands