// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#pragma once

#include <memory>

#include "../../../../core/scripting/angelscript/proxy/ref.hpp"
#include "../../../environment/player.hpp"

namespace game::scripting::angelscript::proxy {
    class Player : public core::scripting::angelscript::proxy::Ref {
       public:
        explicit Player(std::shared_ptr<game::environment::Player> player)
            : _ptr(std::move(player)) {}
        explicit Player(std::shared_ptr<game::environment::Player> &&player)
            : _ptr(std::move(player)) {}

        void SendChatMessage(uint8_t type, const std::string &message) {
            _ptr->SendChatMessage(type, message);
        }

        std::string GetName() const { return _ptr->GetName(); }

        uint8_t GetLevel() { return _ptr->GetLevel(); }

        void SetLevel(uint8_t level) {
            _ptr->SetLevel(level);
            _ptr->SendCharacterAdditional(_ptr->GetConnection());
        }

       private:
        std::shared_ptr<game::environment::Player> _ptr;
    };
}  // namespace game::scripting::angelscript::proxy