// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "application.hpp"

#include "../core/mysql/migrations/_defaults.hpp"
#include "../core/networking/reserved_headers.hpp"
#include "../core/networking/utils.hpp"
#include "../core/profiler.hpp"
#include "commands/command_manager.hpp"
#include "environment/object.hpp"
#include "formats/mob_proto.hpp"
#include "item/item.hpp"
#include "packets/general.hpp"
#include "scripting/angelscript/interface.hpp"

namespace game {
    std::shared_ptr<Application> Application::_instance = nullptr;

    Application::Application(
        const std::shared_ptr<core::Application> &core_application)
        : _coreApplication(core_application), _port(0) {
        PROFILE_FUNCTION();
        _playerCache = std::make_shared<core::cache::PlayerCache>(
            _coreApplication->GetRedis(), _coreApplication->GetMySQL());

        _accountCache = std::make_shared<core::cache::AccountCache>(
            _coreApplication->GetRedis(), _coreApplication->GetMySQL());
    }

    Application::~Application() = default;

    void Application::Start() {
        PROFILE_FUNCTION();
        assert(_instance == nullptr);
        _instance = shared_from_this();

        // Register interface
#ifdef ENABLE_ANGELSCRIPT
        scripting::angelscript::Interface::RegisterInterface(
            std::static_pointer_cast<core::scripting::angelscript::AngelScript>(
                _coreApplication->GetScriptEngine("AngelScript")));
#endif

        auto conf = _coreApplication->GetConfiguration();

        _port = conf.get<uint16_t>("port");

        // Create server
        try {
            _server = std::make_shared<core::networking::Server>(
                _coreApplication, conf.get<std::string>("host", "*"), _port,
                conf.get<uint8_t>(
                    "securityLevel",
                    core::networking::SECURITY_LEVEL_KEY_AGREEMENT),
                reinterpret_cast<const uint32_t *>(
                    conf.get<std::string>("pong", "testtesttesttest").c_str()));
        } catch (const boost::system::system_error &err) {
            CORE_LOGGING(fatal) << "Failed to create server " << err.what();
            return;
        }

        // Register packets
        RegisterPackets();

        _itemManager = std::make_shared<item::ItemManager>(
            _coreApplication->GetRedis(), _coreApplication->GetMySQL());
        _itemManager->Load();

        formats::MobProto::Load("mob_proto", _monsters);

        _animationManager = std::make_shared<formats::AnimationManager>();
        _animationManager->Load();

        commands::CommandManager::RegisterDefaultCommands();

        _world = std::make_shared<environment::World>();
        _world->Load();

        CORE_LOGGING(info) << "Starting game server";
        _server->Run();
    }

    void Application::Update(uint32_t elapsedTime) {
        PROFILE_FUNCTION();

        _world->Update(elapsedTime);
    }

    void Application::OnNewConnection(
        std::shared_ptr<core::networking::Connection> connection) {
        connection->SetPhase(core::networking::Phases::PHASE_LOGIN);
    }

    void Application::OnDisconnect(
        std::shared_ptr<core::networking::Connection> connection) {
        if (connection->HasProperty("login_key")) {
            _playerCache->SetLoginKeyTTL(
                connection->GetProperty<uint32_t>("login_key"));
        }

        auto player = connection->GetPlayer();
        connection->SetPlayer(nullptr);

        if (player) {
            player->SetConnection(nullptr);
            auto map = player->GetMap();
            if (map) {
                map->DespawnObject(player);
            }
        }
    }

    void Application::LoginRequest(
        std::shared_ptr<core::networking::Connection> connection,
        std::shared_ptr<core::networking::Packet> packet) {
        auto key = packet->GetField<uint32_t>("key");
        auto username = packet->GetString("username");
        CORE_LOGGING(trace)
            << "Key based login for " << username << " using key " << key;

        auto accountId = _playerCache->CheckLoginKey(key);
        if (accountId == 0) {
            CORE_LOGGING(warning) << "Invalid login key " << key;
            connection->PostClose();
            return;
        }
        _playerCache->RemoveLoginKeyTTL(key);
        connection->SetProperty("account_id", accountId);
        connection->SetProperty("login_key", key);

        if (connection->GetSecurityLevel() ==
            core::networking::SECURITY_LEVEL_XTEA) {
            uint32_t *xteakeys = (uint32_t *)packet->GetField("xteakeys");
            connection->ChangeXTEAKey(xteakeys);
        }

        auto players = _playerCache->GetPlayers(accountId);

        // Send player empire to client
        auto empirePacket = _server->GetPacketManager()->CreatePacket(
            0x5a, core::networking::Outgoing);

        auto empire = _accountCache->GetEmpire(accountId);

        if (empire == 0) {
            empirePacket->SetField<uint8_t>("empire", 1);
        } else {
            empirePacket->SetField<uint8_t>("empire", empire);
        }
        connection->Send(empirePacket);

        // Enter character selection
        connection->SetPhase(core::networking::Phases::PHASE_SELECT);

        // Calculate ip address of this server
        auto config = _coreApplication->GetConfiguration();
        std::string localAddress;
        if (config.count("ip") != 0) {
            localAddress = config.get<std::string>("ip");
        } else {
            localAddress = core::networking::GetLocalAddress();
        }
        auto localIp = core::networking::ConvertIP(localAddress);

        // Create packet with player characters
        auto characters = _server->GetPacketManager()->CreatePacket(
            0x20, core::networking::Outgoing);
        auto index = 0;
        for (auto playerId : players) {
            // Query player from cache
            auto player = _playerCache->GetPlayer(playerId);
            if (player.id > 0) {
                auto packetPlayer =
                    characters->GetRepeatedSubField("characters", index);
                packetPlayer->SetField<uint32_t>("id", player.id);
                packetPlayer->SetString("name", player.name);
                packetPlayer->SetField<uint8_t>("class", player.playerClass);
                packetPlayer->SetField<uint32_t>("playtime", player.playtime);
                packetPlayer->SetField<uint8_t>("level", player.level);
                packetPlayer->SetField<uint8_t>("st", player.st);
                packetPlayer->SetField<uint8_t>("ht", player.ht);
                packetPlayer->SetField<uint8_t>("dx", player.dx);
                packetPlayer->SetField<uint8_t>("iq", player.iq);
                packetPlayer->SetField<uint16_t>("bodyPart", 11830);  // todo
                packetPlayer->SetField<uint16_t>("hairPart", 4010);   // todo
                packetPlayer->SetField<int32_t>("posX", player.posX);
                packetPlayer->SetField<int32_t>("posY", player.posY);
                packetPlayer->SetField<int32_t>("ip", localIp);   // todo
                packetPlayer->SetField<uint16_t>("port", _port);  // todo
            }
            index++;
        }
        connection->Send(characters);
    }

    void Application::CharacterSelect(
        std::shared_ptr<core::networking::Connection> connection,
        std::shared_ptr<core::networking::Packet> packet) {
        // Make sure we have an account id
        if (!connection->HasProperty("account_id")) {
            CORE_LOGGING(warning) << "Character select without account id";
            connection->PostClose();
            return;
        }

        auto slot = packet->GetField<uint8_t>("slot");
        CORE_LOGGING(trace) << "Selected character slot " << (int)slot;

        auto players = _playerCache->GetPlayers(
            boost::get<uint32_t>(connection->GetProperty("account_id")));
        if (slot >= players.size()) {
            CORE_LOGGING(warning) << "Slot index out of bounds";
            connection->PostClose();
            return;
        }

        connection->SetPhase(core::networking::Phases::PHASE_LOADING);

        auto player = _world->CreatePlayer(players[slot], connection);
        player->Load();
        connection->SetProperty("player_id", players[slot]);
        connection->SetPlayer(player);

        // Basic character details
        player->SendBasicData();
        // Send point data (including exp, level etc etc)
        player->SendPoints();

        // todo: Send skill data
    }

    void Application::SelectEmpire(
        std::shared_ptr<core::networking::Connection> connection,
        std::shared_ptr<core::networking::Packet> packet) {
        CORE_LOGGING(trace)
            << "Empire selected: "
            << static_cast<uint32_t>(packet->GetField<uint8_t>("empire"));
        connection->SetProperty("empire", packet->GetField<uint8_t>("empire"));
    }

    void Application::CreateCharacter(
        std::shared_ptr<core::networking::Connection> connection,
        std::shared_ptr<core::networking::Packet> packet) {
        auto stmt = _coreApplication->GetMySQL()->CreateStatement(
            "INSERT INTO `" GAME_DATABASE
            "`.`players` (account_id, name, class, appearance) VALUES (%1%, "
            "%4%, %2%, %3%)");

        auto accountId = connection->GetProperty<uint32_t>("account_id");
        stmt << accountId;

        auto classField = packet->GetField<uint16_t>("class");
        auto appearance = packet->GetField<uint8_t>("appearance");

        stmt << classField;
        stmt << appearance;
        stmt << packet->GetString("name");

        stmt();

        stmt = _coreApplication->GetMySQL()->CreateStatement(
            "SELECT LAST_INSERT_ID() AS id");

        auto result = stmt();
        result->Next();

        auto playerId = result->GetUnsigned32("id");

        stmt = _coreApplication->GetMySQL()->CreateStatement(
            "INSERT INTO `" GAME_DATABASE
            "`.`player_data` (player_id, x, y, hp, mp, stamina, st, ht, dx, "
            "iq, level, gold, last_play) VALUES "
            "(%1%, 332414, 756489, 100, 100, 100, 99, 99, 99, 99, 99, 300000, "
            "NOW())");

        stmt << playerId;

        stmt();

        if (connection->HasProperty("empire")) {
            auto empire = connection->GetProperty<uint8_t>("empire");
            stmt = _coreApplication->GetMySQL()->CreateStatement(
                "UPDATE `" ACCOUNT_DATABASE
                "`.`accounts` SET `empire` = %2% WHERE `id` = %1%");

            stmt << accountId;
            stmt << empire;
            stmt();
        }

        auto player = _playerCache->GetPlayer(playerId);

        _playerCache->AddPlayer(accountId, playerId);

        auto success = _server->GetPacketManager()->CreatePacket(
            0x08, core::networking::Outgoing);

        success->SetField<uint8_t>("slot", packet->GetField<uint8_t>("slot"));

        auto packetPlayer = success->GetSubField("character");
        packetPlayer->SetField<uint32_t>("id", player.id);
        packetPlayer->SetString("name", player.name);
        packetPlayer->SetField<uint8_t>("class", player.playerClass);
        packetPlayer->SetField<uint32_t>("playtime", player.playtime);
        packetPlayer->SetField<uint8_t>("level", player.level);
        packetPlayer->SetField<uint8_t>("st", player.st);
        packetPlayer->SetField<uint8_t>("ht", player.ht);
        packetPlayer->SetField<uint8_t>("dx", player.dx);
        packetPlayer->SetField<uint8_t>("iq", player.iq);
        packetPlayer->SetField<uint16_t>("bodyPart", 11830);  // todo
        packetPlayer->SetField<uint16_t>("hairPart", 4010);   // todo
        packetPlayer->SetField<int32_t>("posX", player.posX);
        packetPlayer->SetField<int32_t>("posY", player.posY);

        // Calculate ip address of this server
        auto localAddress = core::networking::GetLocalAddress();
        auto localIp = core::networking::ConvertIP(localAddress);

        packetPlayer->SetField<int32_t>("ip", localIp);   // todo
        packetPlayer->SetField<uint16_t>("port", _port);  // todo

        connection->Send(success);
    }

    void Application::EnterGame(
        std::shared_ptr<core::networking::Connection> connection,
        std::shared_ptr<core::networking::Packet> packet) {
        auto player = connection->GetPlayer();
        if (!player) {
            CORE_LOGGING(error) << "EnterGame without having a player";
            connection->PostClose();
            return;
        }

        connection->SetPhase(core::networking::Phases::PHASE_GAME);

        auto time = _server->GetPacketManager()->CreatePacket(
            0x6a, core::networking::Outgoing);
        time->SetField<uint32_t>("time", _coreApplication->GetCoreTime());
        connection->Send(time);

        auto channel = _server->GetPacketManager()->CreatePacket(
            0x79, core::networking::Outgoing);
        channel->SetField<uint8_t>("channel", 1);
        connection->Send(channel);

        CORE_LOGGING(trace) << "Spawn player " << player->GetVID();
        _world->SpawnObject(player);
        player->Show(connection);

        player->SendInventory();

        // Send welcome message
        auto welcome = _server->GetPacketManager()->CreatePacket(
            0x04, core::networking::Outgoing);
        welcome->SetField<uint8_t>("messageType", 2);
        welcome->SetDynamicString("Powered by QuantumCore");
        connection->Send(welcome);
    }

#define PLAYER_HANDLER(header, method)                                       \
    _server->RegisterHandler(                                                \
        header, [](std::shared_ptr<core::networking::Connection> connection, \
                   std::shared_ptr<core::networking::Packet> packet) {       \
            auto player = connection->GetPlayer();                           \
            if (!player) {                                                   \
                CORE_LOGGING(trace) << "No player on connection object";     \
                connection->PostClose();                                     \
                return;                                                      \
            }                                                                \
            player->method(packet);                                          \
        })

    void Application::RegisterPackets() {
        _server->SetNewConnectionHandler(std::bind(
            &Application::OnNewConnection, this, std::placeholders::_1));
        _server->SetDisconnectHandler(
            std::bind(&Application::OnDisconnect, this, std::placeholders::_1));
        packets::RegisterGeneral(_server->GetPacketManager());

        _server->RegisterHandler(
            0x6d, std::bind(&Application::LoginRequest, this,
                            std::placeholders::_1, std::placeholders::_2));
        _server->RegisterHandler(
            0x06, std::bind(&Application::CharacterSelect, this,
                            std::placeholders::_1, std::placeholders::_2));
        _server->RegisterHandler(
            0xf1, [](std::shared_ptr<core::networking::Connection> connection,
                     std::shared_ptr<core::networking::Packet> packet) {
                CORE_LOGGING(trace) << packet->GetString("name") << " "
                                    << packet->GetString("timestamp");
            });
        _server->RegisterHandler(
            0x0a, std::bind(&Application::EnterGame, this,
                            std::placeholders::_1, std::placeholders::_2));

        _server->RegisterHandler(
            0x5a, std::bind(&Application::SelectEmpire, this,
                            std::placeholders::_1, std::placeholders::_2));
        _server->RegisterHandler(
            0x04, std::bind(&Application::CreateCharacter, this,
                            std::placeholders::_1, std::placeholders::_2));

        PLAYER_HANDLER(0x0d, OnItemMove);
        PLAYER_HANDLER(0x03, OnChat);
        PLAYER_HANDLER(0x07, OnMove);
    }
    std::shared_ptr<Application> Application::GetInstance() {
        assert(_instance != nullptr);
        return _instance;
    }
}  // namespace game